# AlterNumEduc

Dépôt du projet AlterNumEduc : **Alter**natives **Num**ériques dans l'**Éduc**ation, promotion, déploiement des logiciels libres dans les établissements scolaires.

Le site web : https://alternumeduc.tricassinux.org


# Logiciels liés à l'enseignement potentiellement utilisés pour le projet

- Voir la page : https://alternumeduc.tricassinux.org/index.php?page=Logiciels


# Réemploi de matériel avec une mise à jour du système avec GNU/Linux

Pour des raisons écologiques et aussi pour obtenir des ordinateurs et/ou des laboratiores informatiques de qualité à des prix abordables, voir cet exemple de réemploi d'ordinateurs avec Emmabuntüs (https://emmabuntus.org) 

Plus particulièrement le projet "Campagne de réemploi Debian-Facile & emmabuntus" : https://emmabuntus.org/la-cle-usb-magique-ou-presque-demmabuntus-pour-le-reemploi-dordinateur/.

Pour plus d'information sur le réemploi de matériel et la mise à jour du systèmes d'exploitation avec GNU/Linux, n'hésitez pas à contacter l'équipe d'AlterNumEduc : [Lien vers le formulaire](https://alternumeduc.tricassinux.org/contact_form.php "Contacter l'équipe d'AlterNumEduc").

